<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\User;
use Form;
use Html;
use Session;


class UserController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

        public function register(Request $request){
        	$this->validate($request,[
	         'username'=>'required|max:200|alpha',
	         'email' => 'required|email',
	         'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
	         'password_confirmation' => 'min:6',
	         'terms'=>'required'
	        ]);

	        $user = new User([
				'name' => $request->get('username'),
				'email' => $request->get('email'),
				'password' => bcrypt($request->get('password')),

			]);
			$user->save();

			/*Session::flash('message','Successfully Registered!!');
    		return redirect('login');*/
        }


        public function register2(Request $request){
        	$this->validate($request,[
	         'username'=>'required|max:200|alpha',
	         'email' => 'required|email',
	         'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
	         'password_confirmation' => 'min:6',
	         'terms'=>'required'
	        ]);

        	// dd($request);
	        $user = new User([
				'name' => $request->get('username'),
				'email' => $request->get('email'),
				'password' => bcrypt($request->get('password')),

			]);
			$user->save();

			Session::flash('message','Successfully Registered!!');
    		return redirect('login');
        }

}


