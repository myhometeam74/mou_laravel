@extends('layouts.custAdd')
@section('content')
	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>Creative SignUp Form</h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
				@if (count($errors) > 0)
			         <div class = "alert alert-danger">
			            <ul>
			               @foreach ($errors->all() as $error)
			                  <li style="color: #f00;">{{ $error }}</li>
			               @endforeach
			            </ul>
			         </div>
			      @endif
			      @if(Session::has('message'))
					<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
				  @endif

				<!-- <form action="{{ url('register') }}" method="get"> -->
				{{ Form::open(array('url'=>'register2', 'method'=>'post', 'id'=>'registration_form')) }}
					<label>Name</label>
					<input class="text required" type="text" name="username" placeholder="Username">
					<label>Email</label>
					<input class="text email" type="email" name="email" placeholder="Email">
					<label>Password</label>
					<input class="text required" type="password" name="password" placeholder="Password">
					<label>Confirm Password</label>
					<input class="text w3lpass required" type="password" name="password_confirmation" placeholder="Confirm Password" >
					<div class="wthree-text">
						<label class="anim">
							<input type="checkbox" class="checkbox" name="terms">
							<span>I Agree To The Terms & Conditions</span>
						</label>
						<div class="clear"> </div>
					</div>
					<!-- <input type="submit" value="SIGNUP"> -->
	
				{{ Form::submit('SIGNUP') }}
					<!-- <input type="button" value="SIGNUP"> -->
				{{ Form::close() }}
				<p>Don't have an Account? <a href="{{url('login')}}"> Login Now!</a></p>
			</div>
		</div>
@endsection

@push('css')
<style type="text/css">
.error
{
	color: red;
}
.success
{
	color: blue;
}
</style>
@endpush()


