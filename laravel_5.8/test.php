@extends('layouts.custAdd')
@section('content')
	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>Creative SignUp Form</h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
				@if (count($errors) > 0)
			         <div class = "alert alert-danger">
			            <ul>
			               @foreach ($errors->all() as $error)
			                  <li style="color: #f00;">{{ $error }}</li>
			               @endforeach
			            </ul>
			         </div>
			      @endif
			      		@if(Session::has('message'))
				          <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
				        @endif

				{{ Form::open(array('url'=>'login','method'=>'post', 'id'=>'registration_form','role'=>'form')) }}
					<label>Name</label>
					<input class="form-control" type="text" name="username" placeholder="User name">
					 @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                      @endif
					<label>Email</label>
					<input class="form-control" type="email" name="email" placeholder="Email">
					@if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                      @endif
					<label>Password</label>
					<input class="form-control" type="password" name="password" placeholder="Password">
					@if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                      @endif
					<label>Confirm Password</label>
					<input class="form-control" type="password" name="password_confirmation" placeholder="Confirm Password" >
					@if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                      @endif
					<div class="wthree-text">
						<label class="anim">
							<input type="checkbox" class="" name="terms">
							@if ($errors->has('terms'))
                        <span class="help-block">
                            <strong>{{ $errors->first('terms') }}</strong>
                        </span>
                      @endif
							<span>I Agree To The Terms & Conditions</span>
						</label>
						<div class="clear"> </div>
					</div>
					<input type="submit" value="SIGNUP">
				<!-- <button class="btn btn-primary btn-user" name="add_category_btn" id="category-form-submit-btn">Submit</button> -->

				{{ Form::close() }}
				<p>Don't have an Account? <a href="{{url('login')}}"> Login Now!</a></p>
			</div>
		</div>
@endsection
		



		